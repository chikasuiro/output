Param([String]$infile)

if (-not $infile) {
  Write-Host "Usage: pwsh ./fistr_result_converter.ps1 [infile]"
  exit
}

$incontent = Get-Content $infile
$nline = $incontent.Length

$ln_data = 0
for ($i = 0; $i -lt $nline; $i++) {
  if ($incontent[$i] -ceq "*data") {
    $ln_data = $i + 1
    break
  }
}

$outfile = $infile.Replace(".res", "_v1.res")
$outcontent = @()
$outcontent += "*fstrresult"
$outcontent += $incontent[$ln_data..($nline-1)]

$enc_utf8 = New-Object System.Text.UTF8Encoding $false
[System.IO.File]::WriteAllLines($outfile, $outcontent, $enc_utf8)
# $enc_sjis = [Text.Encoding]::GetEncoding(932)
# [System.IO.File]::WriteAllLines($outfile, $outcontent, $enc_sjis)
