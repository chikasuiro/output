import sys

if not len(sys.argv) == 2:
    print('Usage: python fistr_result_converter.py [file]')
    exit()

infile = sys.argv[1]
with open(infile, 'r') as f:
    incontent = f.readlines()

ln_data = 0
for i in range(len(incontent)):
    if incontent[i].strip() == '*data':
        ln_data = i + 1
        break

outfile = infile.replace('.res', '_v1.res')
outcontent = ['*fstrresult\n'] + incontent[ln_data:]
with open(outfile, 'w') as f:
    f.writelines(outcontent)
