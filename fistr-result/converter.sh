#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: ./converter.sh [infile]"
    exit
fi

infile=$1
outfile=${infile//.res/_v1.res}
echo "*fstrresult" > $outfile
tail -n +10 $infile >> $outfile
